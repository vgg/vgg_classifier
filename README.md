VGG Image Classifier Backend Service
====================================

Derived from the [cpuvisor-srv](https://github.com/kencoken/cpuvisor-srv) project by Ken Chatfield, University of Oxford.

License: BSD (see LICENSE.md)

Supported platforms
-------------------

Successfully compiled on Ubuntu and macOS Sierra. See the files in the `install` directory for example installation scripts on each platform.

Installation Instructions
-------------------------

The CMAKE package is used for installation. Ensure you have all dependencies
installed from the section below, and then go to the root folder of vgg_classifier and type:

    $ mkdir build
    $ cd build
    $ cmake -DCMAKE_CXX_STANDARD=11 -DLiblinear_DIR=<liblinear-root-dir> -Dcppnetlib_DIR=<cppnetlib-root-dir>/build/ ../
    $ make
    $ make install

If you want to use a specific Boost library, you can replace the third line above with

    $ cmake -DCMAKE_CXX_STANDARD=11 -DBOOST_ROOT=<boost-root-dir> -DLiblinear_DIR=<liblinear-root-dir> -Dcppnetlib_DIR=<cppnetlib-root-dir>/build/ ../

If you want to install on a specific directory, use the
`CMAKE_INSTALL_PREFIX` option, like so:

    $ cmake -DCMAKE_INSTALL_PREFIX=/path/to/install/dir ...

As in the original [cpuvisor-srv](https://github.com/kencoken/cpuvisor-srv)  project, the generated executables still use the `cpuvisor-srv` prefix.

Usage
-----

The following preprocessing steps are required before running the service:

 1. Edit `./config.prototxt` with Caffe model files and dataset base paths.
 2. Create/Edit `./dsetpaths.txt` and `./negpaths.txt` with the paths to all dataset and negative training images. These files should be referenced in `./config.prototxt`. Sample files are provided (`./dsetpaths_sample.txt` contains all images from the PASCAL VOC 2007 dataset).
 3. Run `cpuvisor_preproc` to precompute all features.
 4. Run `cpuvisor_legacy_serve.py` and `cpuvisor_service` to start the backend service that interfaces with the [vgg_frontend](https://gitlab.com/vgg/vgg_frontend) web application.

Wiki
----

The [Wiki](https://gitlab.com/vgg/vgg_classifier/wikis/home) explains the details about the communication API used in the service, as well as it includes other useful links and information.

Dependencies
------------

#### 1. VGG Image Classifier Service Dependencies

The following C++ libraries are required:

 + [Caffe](http://caffe.berkeleyvision.org/)
 + [cppnetlib](https://github.com/cpp-netlib/cpp-netlib)
 + Boost v1.55.0 or above (successfully tested with v1.57)
 + [Liblinear](http://www.csie.ntu.edu.tw/~cjlin/liblinear/) (successfully tested with v2.1)
 + ZeroMQ

The following are dependencies shared with Caffe. If you have successfully compiled Caffe these dependencies are already in your system. Otherwise see http://caffe.berkeleyvision.org/install_apt.html.

 + OpenCV
 + Google Logging (GLOG)
 + Google Flags (GFLAGS) v2.1+
 + Google Protobuf

Some additional python dependencies for the scripts in the `pyclient` and `test`:

 + protobuf==3.0.0
 + pyzmq==17.1.2
 + gevent==0.13.8 greenlet==0.4.15
 + numpy==1.11.1 matplotlib==1.5.3 (for the tests)
