#include <limits>
#include <glog/logging.h>
#include <gflags/gflags.h>

#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

#include "directencode/caffe_encoder.h"
#include "server/util/preproc.h"
#include "server/util/io.h"

#include "cpuvisor_config.pb.h"

DEFINE_bool(dsetfeats, false, "Compute dataset features");
DEFINE_bool(negfeats, false, "Compute negative training image features");
DEFINE_bool(force, false,
            "Force computation of features even if feature file exists");

DEFINE_int64(startidx, -1, "Starting index for dataset feature computation (inclusive index)");
DEFINE_int64(endidx, -1, "Ending index for dataset feature computation (exclusive index)");


int main(int argc, char* argv[]) {

  google::InitGoogleLogging(argv[0]);

  google::InstallFailureSignalHandler();
  google::SetUsageMessage("Preprocessing for CPU Visor server");
  google::ParseCommandLineFlags(&argc, &argv, true);
  if (argc < 2)
    LOG(FATAL) << "Missing required argument CONFIG-PATH";

  cpuvisor::Config config;
  cpuvisor::readProtoFromTextFile(argv[argc-1], &config);

  const cpuvisor::CaffeConfig& caffe_config = config.caffe_config();
  featpipe::CaffeEncoder encoder(caffe_config);

  const cpuvisor::PreprocConfig& preproc_config = config.preproc_config();

  if (FLAGS_dsetfeats) {
    // On the command-line we allow "-1" so that we kow if we need to
    // manipulate the filename but internally indices unsigned.  But
    // this filepath manipulation does not feel right and use of
    // signed int prevents the use of the whole type range when we
    // need it the most, i.e., when we have a lot of images to
    // process.
    uint64_t start_idx = 0;
    uint64_t end_idx = std::numeric_limits<int64_t>::max();

    const bool using_idx = FLAGS_startidx > -1;
    if (using_idx && ! FLAGS_endidx > -1)
      LOG(FATAL) << "If using start/end idx for partial preproccessing,"
                 << " both start/end idx must be used.";

    fs::path feats_file {preproc_config.dataset_feats_file()};
    if (using_idx) {
      start_idx = FLAGS_startidx;
      end_idx = FLAGS_endidx;

      feats_file = vic::path_with_idx_suffix(feats_file, start_idx, end_idx);
    }

    DLOG(INFO) << "Feats file is: " << feats_file;

    if (! FLAGS_force && fs::exists(feats_file)) {
      LOG(INFO) << "Skipping existing feature file!";
    } else {
      cpuvisor::procTextFile(preproc_config.dataset_im_paths(),
                             feats_file.string(),
                             encoder,
                             preproc_config.dataset_im_base_path(),
                             start_idx, end_idx);
    }
  }

  if (FLAGS_negfeats) {
    DLOG(INFO) << "Neg feats file is: " << preproc_config.neg_feats_file();

    if (! FLAGS_force && fs::exists(preproc_config.neg_feats_file())) {
      LOG(INFO) << "Skipping existing feature file!";
    } else {
      cpuvisor::procTextFile(preproc_config.neg_im_paths(),
                             preproc_config.neg_feats_file(),
                             encoder,
                             preproc_config.neg_im_base_path());
    }
  }

  return 0;
}
