////////////////////////////////////////////////////////////////////////////
//    File:        query_data.h
//    Author:      Ken Chatfield
//    Description: Data associated with a query
////////////////////////////////////////////////////////////////////////////

#ifndef CPUVISOR_QUERY_DATA_H_
#define CPUVISOR_QUERY_DATA_H_

#include <vector>
#include <string>
#include <opencv2/opencv.hpp>

#include <boost/thread.hpp>

namespace cpuvisor {

  struct Ranking {
    cv::Mat scores;
    cv::Mat sort_idxs;
  };

  enum QueryState {QS_DATACOLL, QS_DATACOLL_COMPLETE,
                   QS_TRAINING, QS_TRAINED,
                   QS_RANKING, QS_RANKED};

  struct QueryData {
    // Use a map with filenames for debug purposes.  Use a map instead
    // of unordered_map because keeping the same order helps in
    // keeping reproducible results (see #30).
    std::map<std::string, cv::Mat> pos;
    boost::mutex pos_mutex; // to ensure features are added in thread-safe manner
    cv::Mat model;
    Ranking ranking;
  };

  struct QueryIfo {
    QueryIfo() : state(QS_DATACOLL) {}
    QueryIfo(const std::string& id)
      : id(id)
      , state(QS_DATACOLL) { }
    std::string id;
    QueryState state;
    QueryData data;
  };

}

#endif
