#include "base_server.h"

#include <fstream>

#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

#include <boost/filesystem.hpp>
namespace fs = boost::filesystem;

#include <boost/thread.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <opencv2/imgcodecs.hpp>

#include "server/util/io.h"
#include "server/util/feat_util.h"

#ifdef MATEXP_DEBUG
  #include "server/util/debug/matfileutils_cpp.h"
#endif


bool path_ends_in_separator(const std::string& path) {
  return path[path.length() -1] == fs::path::preferred_separator;
}


namespace cpuvisor {

  void
  BaseServer::process(const std::string& id, const std::string& imfile)
  {
    std::shared_ptr<QueryIfo> query_ifo = getQueryIfo_(id);

    DLOG(INFO) << "Postprocessing image: " << imfile;
    if (query_ifo->state != QS_DATACOLL) {
      LOG(INFO) << "Skipping computing feature(s) for query " << query_ifo->id << " as it has advanced past data collection stage";
      return;
    }

    cv::Mat feat;
    try {
      feat = computeFeat_(imfile);
    } catch (featpipe::InvalidImageError& e) {
      LOG(INFO) << "Failed to process invalid image '" << imfile << "': " << e.what();
      return;
    }

    if (query_ifo->state != QS_DATACOLL) {
      LOG(INFO) << "Skipping adding feature(s) for query " << query_ifo->id << " as it has advanced past data collection stage";
      return;
    }

    {
      // While there's little point on adding the same image twice,
      // there is nothing else stopping someone from trying.
      boost::mutex::scoped_lock lock(query_ifo->data.pos_mutex);
      CHECK(query_ifo->data.pos.find(imfile) == query_ifo->data.pos.end())
        << "image file '" << imfile << "' already added";
      query_ifo->data.pos[imfile] = feat;
    }

    notifier_->post_image_processed_(query_ifo->id, imfile);
  }

  cv::Mat BaseServer::computeFeat_(const std::string& imfile) {

    // look for precomputed feature from dataset first
    {
      bool proc_rel_path = true;

      // get relative path from full path
      std::string rel_path = imfile;
      DLOG(INFO) << "dset_base_path is: '" << dset_base_path_ << "'";
      if (!dset_base_path_.empty()) {
        size_t base_path_idx = imfile.find(dset_base_path_);
        if (base_path_idx == std::string::npos) {
          proc_rel_path = false;
        } else {
          size_t start_idx = base_path_idx + dset_base_path_.length();
          DLOG(INFO) << "Trimming rel_path using: " << dset_base_path_ << " " << start_idx;
          rel_path = imfile.substr(start_idx);
          if (rel_path[0] == fs::path("/").make_preferred().string()[0]) {
            // trim leading dir separator
            rel_path = rel_path.substr(1);
          }
          DLOG(INFO) << "Trimmed rel_path is: " << rel_path;
        }
      }

      if (proc_rel_path) {
        DLOG(INFO) << "rel_path to find is: " << rel_path;
        std::map<std::string, size_t>::const_iterator it =
          dset_paths_index_.find(rel_path);
        if (it != dset_paths_index_.end()) {
          CHECK_EQ(dset_paths_[it->second], rel_path);

          LOG(INFO) << "Looking up feature from dataset: " << rel_path;
          cv::Mat feat = dset_feats_.row(it->second);
          return feat;
        } else {
          LOG(WARNING) << "Path looked like dataset image, but could not be found in dataset index: " << rel_path;
        }
      }

    }

    // if it isn't a dataset image, compute directly as normal
    DLOG(INFO) << "Computing feature from scratch...";
    cv::Mat im = cv::imread(imfile, cv::ImreadModes::IMREAD_COLOR);
    im.convertTo(im, CV_32FC3);

    std::vector<cv::Mat> ims;
    ims.push_back(im);
    cv::Mat feat = encoder_.compute(ims);

    return feat;
  }

  // -----------------------------------------------------------------------------

  BaseServer::BaseServer(const cpuvisor::Config& config)
    : encoder_ {config.caffe_config()} {

    LOG(INFO) << "Load in features...";

    const cpuvisor::PreprocConfig preproc_config = config.preproc_config();

    CHECK(cpuvisor::readFeatsFromProto(preproc_config.dataset_feats_file(),
                                       &dset_feats_, &dset_paths_));
    dset_base_path_ = preproc_config.dataset_im_base_path();

    CHECK(cpuvisor::readFeatsFromProto(preproc_config.neg_feats_file(),
                                       &neg_feats_, &neg_paths_));
    neg_base_path_ = preproc_config.neg_im_base_path();

    for (size_t i = 0; i < dset_paths_.size(); ++i) {
      dset_paths_index_.insert(std::pair<std::string, size_t>(dset_paths_[i], i));
    }

    const cpuvisor::ServerConfig server_config = config.server_config();

    image_cache_path_ = server_config.image_cache_path();
    notifier_ = std::unique_ptr<StatusNotifier>(new StatusNotifier());
  }

  std::string BaseServer::startQuery() {
    static boost::uuids::random_generator uuid_gen;

    std::string id;
    do {
      LOG(INFO) << "Geneating UUID for Query ID";
      id = boost::uuids::to_string(uuid_gen());
    } while (queries_.find(id) != queries_.end());

    DLOG(INFO) << "Starting query (" << id << ")";
    std::shared_ptr<QueryIfo> query_ifo(new QueryIfo(id));
    queries_[id] = query_ifo;

    notifier_->post_state_change_(id, query_ifo->state);

    return id;
  }

  void BaseServer::trainAndRank(const std::string& id, const bool block,
                                Ranking* ranking) {
    if ((!block) && (ranking)) {
      throw CannotReturnRankingError("Cannot retrieve ranked list directly unless block = true");
    }

    train(id, block);
    rank(id, block);

    if (ranking) {
      CHECK_EQ(block, true);

      (*ranking) = getRanking(id);
    }
  }

  void BaseServer::train(const std::string& id, const bool block) {
    {
      // check features exist for training
      std::shared_ptr<QueryIfo> query_ifo = getQueryIfo_(id);
      if (query_ifo->data.pos.size() == 0) {
        throw InvalidRequestError("No training images in pos map");
      }
    }


    if (block) {
      train_(id);
    } else {
      boost::thread proc_thread(&BaseServer::train_, this, id);
    }
  }

  void BaseServer::rank(const std::string& id, const bool block) {
    if (block) {
      rank_(id);
    } else {
      boost::thread proc_thread(&BaseServer::rank_, this, id);
    }
  }

  Ranking BaseServer::getRanking(const std::string& id) {
    std::shared_ptr<QueryIfo> query_ifo = getQueryIfo_(id);

    if (query_ifo->state != QS_RANKED) {
      throw WrongQueryStatusError("Cannot test unles state = QS_TRAINED");
    }

    CHECK(!query_ifo->data.ranking.scores.empty());

    return query_ifo->data.ranking;
  }

  void BaseServer::freeQuery(const std::string& id) {
    std::shared_ptr<QueryIfo> query_ifo = getQueryIfo_(id);

    size_t num_erased = queries_.erase(id);

    if (num_erased == 0) throw InvalidRequestError("Tried to free query which does not exist");
  }

  // Legacy methods --------------------------------------------------------------

  void BaseServer::addTrsFromFile(const std::string& id,
                                  const std::vector<std::string>& paths,
                                  const bool block) {
    std::shared_ptr<QueryIfo> query_ifo = getQueryIfo_(id);

    if (paths.size() == 0) throw InvalidRequestError("No paths specified in paths array");

    if (query_ifo->state != QS_DATACOLL) {
      LOG(INFO) << "Skipping adding paths(s) for query " << id << " as it has advanced past data collection stage";
      return;
    }

    if (block) {
      addTrsFromFile_(id, paths);
    } else {
      boost::thread proc_thread(&BaseServer::addTrsFromFile_, this, id, paths);
    }
  }

  void BaseServer::saveAnnotations(const std::string& id, const std::string& filename) {
    std::shared_ptr<QueryIfo> query_ifo = getQueryIfo_(id);

    LOG(INFO) << "Saving annotation file to: " << filename << "...";

    const std::map<std::string, cv::Mat>& pos = query_ifo->data.pos;

    std::ofstream annofile(filename.c_str());

    size_t base_path_len = image_cache_path_.length();
    if (! path_ends_in_separator(image_cache_path_))
      base_path_len += 1;

    for (auto iter = pos.begin(); iter != pos.end(); ++iter) {
      const std::string& pos_path = iter->first;
      // get relative path from full path (a bit of a hack below!)
      std::string rel_path = pos_path;

      std::vector<std::string> searchstrs;
      searchstrs.push_back("postrainimgs"); // MAGIC CONSTANT
      for (auto path : {image_cache_path_, dset_base_path_}) {
        if (path_ends_in_separator(path))
          searchstrs.push_back(path.substr(0, path.length() -1));
        else
          searchstrs.push_back(path);
      }

      for (size_t ssi = 0; ssi < searchstrs.size(); ++ssi) {
        std::string train_img_searchstr = searchstrs[ssi];
        size_t train_img_searchstr_idx = pos_path.find(train_img_searchstr);
        if (train_img_searchstr_idx != std::string::npos) {
          // relative path after searchstr if found
          size_t start_idx = train_img_searchstr_idx;
          if (ssi > 0) { // include trailing 'postrainimgs' when ssi == 0
            start_idx += train_img_searchstr.length() + 1;
          }

          rel_path = pos_path.substr(start_idx);
          break;
        }
      }

      std::string anno = "+1";
      std::string from_dataset = "-1";
      std::string featfile = "";
      annofile << rel_path << "\t"
               << anno << "\t"
               << from_dataset << "\t"
               << featfile << "\n";

      DLOG(INFO) << "save_anno_file:" << rel_path << " " << anno;
      DLOG(INFO) << "               " << pos_path;
      DLOG(INFO) << "               " << base_path_len << ":" << image_cache_path_;
    }

    annofile.flush();
  }

  void BaseServer::loadAnnotations(const std::string& filename,
                                   std::vector<std::string>* paths,
                                   std::vector<int32_t>* annos) {
    LOG(INFO) << "Loading annotation file from: " << filename << "...";

    std::ifstream annofile(filename.c_str());
    std::string line;

    (*paths) = std::vector<std::string>();
    (*annos) = std::vector<int32_t>();

    while (std::getline(annofile, line)) {
      if (!line.empty()) {
        std::string path;
        int32_t anno;

        std::vector<std::string> elems;
        // split fields
        boost::split(elems, line, boost::is_any_of("\t"));
        // validate input line
        if (elems.size() < 2) {
          throw InvalidAnnoFileError("Could not parse required fields from line of annotation file: " + line);
        }
        // first element is string ID
        path = elems[0];
        // second element is annotation generally +1/-1/0
        if ((elems[1] == "1") || (elems[1] == "+1")) {
          anno = 1;
        } else if (elems[1] == "0") {
          anno = 0;
        } else if (elems[1] == "-1") {
          anno = -1;
        } else {
          throw InvalidAnnoFileError("Unrecognised annotation");
        }

        DLOG(INFO) << "load_anno_file: " << path << " " << anno;
        paths->push_back(path);
        annos->push_back(anno);
      }
    }
  }

  void BaseServer::saveClassifier(const std::string& id, const std::string& filename) {

    std::shared_ptr<QueryIfo> query_ifo = getQueryIfo_(id);

    if (query_ifo->state < QS_TRAINED) {
      throw WrongQueryStatusError("Cannot save classifier unles state = QS_TRAINED or later");
    }
    LOG(INFO) << "Saving classifier to: " << filename << "...";

    cpuvisor::writeModelToProto(query_ifo->data.model, filename);

  }

  void BaseServer::loadClassifier(const std::string& id, const std::string& filename) {

    std::shared_ptr<QueryIfo> query_ifo = getQueryIfo_(id);

    if (query_ifo->state == QS_DATACOLL) {
      query_ifo->state = QS_TRAINING;
      notifier_->post_state_change_(id, query_ifo->state);
    } else {
      throw WrongQueryStatusError("Loading classifiers is only supported for new queries (in state QS_DATACOLL)");
    }

    LOG(INFO) << "Loading classifier from: " << filename << "...";
    cpuvisor::readModelFromProto(filename, &query_ifo->data.model);

    query_ifo->state = QS_TRAINED;
    notifier_->post_state_change_(id, query_ifo->state);

  }

  // Protected methods -----------------------------------------------------------

  std::shared_ptr<QueryIfo> BaseServer::getQueryIfo_(const std::string& id) {
    if (id.empty()) throw InvalidRequestError("No query id specified");

    std::map<std::string, std::shared_ptr<QueryIfo> >::iterator query_iter =
      queries_.find(id);

    if (query_iter == queries_.end()) throw InvalidRequestError("Could not find query id");

    return query_iter->second;
  }

  void BaseServer::train_(const std::string& id) {
    std::shared_ptr<QueryIfo> query_ifo = getQueryIfo_(id);

    try {

      if ((query_ifo->state != QS_DATACOLL) && (query_ifo->state != QS_DATACOLL_COMPLETE)) {
        throw WrongQueryStatusError("Cannot train unles state = QS_DATACOLL or QS_DATACOLL_COMPLETE");
      }

      LOG(INFO) << "Entered train in correct state";
      query_ifo->state = QS_TRAINING;
      notifier_->post_state_change_(id, query_ifo->state);

      const std::map<std::string, cv::Mat>& pos = query_ifo->data.pos;

#ifndef NDEBUG // DEBUG
      DLOG(INFO) << "Will train with features computed from the following positive paths:";
      for (auto iter = pos.begin(); iter != pos.end(); ++iter) {
        DLOG(INFO) << iter->first;
      }
#endif

      double svm_c = 1.0;
      if (pos.size() < 10) {
        svm_c = 10.0;
      }

      cv::Mat pos_feats (pos.size(), encoder_.get_code_size(), CV_32FC1);
      DLOG(INFO) << "Created matrix for positive features with size: "
                 << pos_feats.rows << "x" << pos_feats.cols;
      {
        int i = 0;
        for (auto it = pos.begin(); it != pos.end(); ++it, ++i) {
          DLOG(INFO) << "Pushing feature vector with size: "
                     << it->second.rows << "x" << it->second.cols;
          it->second.copyTo(pos_feats.row(i));
        }
      }

      query_ifo->data.model =
        cpuvisor::trainLinearSvm(pos_feats, neg_feats_, svm_c);

#ifdef MATEXP_DEBUG // DEBUG
      MatFile mat_file("prebasetrain.mat", true);
      mat_file.writeFloatMat("w_vect", (float*)query_ifo->data.model.data,
                             query_ifo->data.model.rows,
                             query_ifo->data.model.cols);
#endif

      query_ifo->state = QS_TRAINED;
      notifier_->post_state_change_(id, query_ifo->state);

    } catch (const cv::Exception& e) {

      notifier_->post_error_(id, std::string("OpenCV Exception: ") + std::string(e.what()));

    } catch (const std::runtime_error& e) {

      notifier_->post_error_(id, std::string("Runtime Exception: ") + std::string(e.what()));

    }

  }

  void BaseServer::rank_(const std::string& id) {
    std::shared_ptr<QueryIfo> query_ifo = getQueryIfo_(id);

    try {

      if (query_ifo->state != QS_TRAINED) {
        throw WrongQueryStatusError("Cannot rank unles state = QS_TRAINED");
      }

      LOG(INFO) << "Entered rank in correct state";
      query_ifo->state = QS_RANKING;
      notifier_->post_state_change_(id, query_ifo->state);

      cpuvisor::rankUsingModel(query_ifo->data.model,
                               dset_feats_,
                               &query_ifo->data.ranking.scores,
                               &query_ifo->data.ranking.sort_idxs);

      query_ifo->state = QS_RANKED;
      notifier_->post_state_change_(id, query_ifo->state);

    } catch (const cv::Exception& e) {

      notifier_->post_error_(id, std::string("OpenCV Exception: ") + std::string(e.what()));

    } catch (const std::runtime_error& e) {

      notifier_->post_error_(id, std::string("Runtime Exception: ") + std::string(e.what()));

    }

  }

  void BaseServer::addTrsFromFile_(const std::string& id,
                                   const std::vector<std::string>& paths) {

    for (size_t i = 0; i < paths.size(); ++i) {
      std::string path = paths[i];
      // check if path is relative (assume it is a dataset path if so)
      {
        fs::path path_fs(path);
        if (!path_fs.has_root_path()) {
          path_fs = fs::path(dset_base_path_) / path_fs;
          path = path_fs.string();
        }
      }

      process(id, path);
    }
  }

}
