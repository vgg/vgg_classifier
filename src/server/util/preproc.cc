#include "preproc.h"

#include <fstream>
#include <vector>

#include <boost/filesystem.hpp>
#include <glog/logging.h>
#include <opencv2/opencv.hpp>

#include "server/util/feat_util.h"
#include "server/util/io.h"

#ifdef MATEXP_DEBUG
  #include "server/util/debug/matfileutils_cpp.h"
#endif


namespace fs = boost::filesystem;


std::vector<std::string>
read_paths(const std::string& text_path,
           const uint64_t start_idx,
           const uint64_t end_idx)
{
  std::vector<std::string> paths;

  std::ifstream imfiles(text_path.c_str());
  std::string imfile;
  int64_t iter_count = 0;
  while (std::getline(imfiles, imfile)) {
    if (iter_count < start_idx) {
      DLOG(INFO) << "Skipping index: " << iter_count << " (< " << start_idx << ")";
      if (! imfile.empty())
        ++iter_count;
      continue;
    }

    if (iter_count == end_idx) {
      DLOG(INFO) << "Breaking, iter_count: " << iter_count << " == " << end_idx;
      break;
    }

    if (! imfile.empty()) {
      DLOG(INFO) << "Pushing back: " << imfile;
      paths.push_back(imfile);
      ++iter_count;
    }
    CHECK_GE(iter_count, 0);
  }
  return paths;
}


void
write_feats_file(const std::string& proto_path,
                 const cv::Mat& feats,
                 const std::vector<std::string>& paths)
{
  // ensure output dir exists
  fs::path proto_dir_fs = fs::path(proto_path).parent_path();
  if (! fs::exists(proto_dir_fs))
    fs::create_directories(proto_dir_fs);

  LOG(INFO) << "Writing features to: " << proto_path;
  cpuvisor::writeFeatsToProto(feats, paths, proto_path);
}


namespace vic {
  fs::path
  path_with_idx_suffix(const fs::path& path,
                       const uint64_t start_idx,
                       const uint64_t end_idx)
  {
    return (path.parent_path()
            / (path.stem().string()
               + "-" + std::to_string(start_idx)
               + "_" + std::to_string(end_idx)
               + path.extension().string()));
  }
}


namespace cpuvisor {

  void procTextFile(const std::string& text_path,
                    const std::string& proto_path,
                    featpipe::CaffeEncoder& encoder,
                    const std::string& base_path,
                    const uint64_t start_idx,
                    const uint64_t end_idx) {

    fs::path base_path_fs(base_path);
    if (!base_path.empty()) {
      CHECK(fs::exists(base_path_fs) && fs::is_directory(fs::canonical(base_path_fs)))
        << "Base path should exist or be blank: " << base_path;
    }

    auto paths = read_paths(text_path, start_idx, end_idx);
    CHECK_GT(paths.size(), 0) << "No paths could be read from file: " << text_path;

    cv::Mat feats (0, encoder.get_code_size(), CV_32F);
    feats.reserve(paths.size());

    std::vector<std::string> valid_paths;
    for (size_t i = 0; i < paths.size(); ++i) {

      try {
          LOG(INFO) << "Computing feature for image: " << paths[i];

          std::string full_path = (base_path_fs / fs::path(paths[i])).string();
          cv::Mat feat = cpuvisor::computeFeat(full_path, encoder);
          feats.push_back(feat);
          valid_paths.push_back(paths[i]);
      }
      catch (featpipe::InvalidImageError& e) {
        // Do not completely abort in this case. Skip the file
        // and leave it out of the features
        LOG(INFO) << "Skipping invalid image '" << paths[i] << "': " << e.what();
      }
    }

    #ifdef MATEXP_DEBUG // DEBUG
    MatFile mat_file(proto_path + ".mat", true);
    mat_file.writeFloatMat("feats", (float*)feats.data, feats.rows, feats.cols);
    mat_file.writeVectOfStrs("paths", valid_paths);
    #endif

    write_feats_file(proto_path, feats, valid_paths);
  }

}
