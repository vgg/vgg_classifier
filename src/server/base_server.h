////////////////////////////////////////////////////////////////////////////
//    File:        base_server.h
//    Author:      Ken Chatfield
//    Description: Server functions
////////////////////////////////////////////////////////////////////////////

#ifndef CPUVISOR_BASE_SERVER_H_
#define CPUVISOR_BASE_SERVER_H_

#include <vector>
#include <string>
#include <map>
#include <memory>
#include <boost/utility.hpp>
#include <opencv2/opencv.hpp>

#include "directencode/caffe_encoder.h"

#include "server/query_data.h" // defines all datatypes used in this class
#include "server/util/status_notifier.h"
#include "cpuvisor_config.pb.h"

namespace cpuvisor {

  // exceptions --------------------------

  class InvalidRequestError: public std::runtime_error {
  public:
    InvalidRequestError(std::string const& msg): std::runtime_error(msg) { }
  };

  class WrongQueryStatusError: public InvalidRequestError {
  public:
    WrongQueryStatusError(std::string const& msg): InvalidRequestError(msg) { }
  };

  class CannotReturnRankingError: public InvalidRequestError {
  public:
    CannotReturnRankingError(std::string const& msg): InvalidRequestError(msg) { }
  };

  class InvalidAnnoFileError: public InvalidRequestError {
    public:
    InvalidAnnoFileError(std::string const& msg): InvalidRequestError(msg) { }
  };

  // callback functor specializations ----

  class BaseServer : boost::noncopyable {
  public:
    BaseServer(const cpuvisor::Config& config);
    virtual ~BaseServer() {};

    virtual std::string startQuery();
    virtual void trainAndRank(const std::string& id, const bool block = false,
                              Ranking* ranking = 0);
    virtual void train(const std::string& id, const bool block = false);
    virtual void rank(const std::string& id, const bool block = false);
    virtual Ranking getRanking(const std::string& id);
    virtual void freeQuery(const std::string& id);

    inline std::shared_ptr<StatusNotifier> notifier() {
      return notifier_;
    }
    inline std::string dset_path(const size_t idx) const {
      CHECK_LT(idx, dset_paths_.size());
      return dset_paths_[idx];
    }

    // legacy methods
    virtual void addTrsFromFile(const std::string& id, const std::vector<std::string>& paths,
                                const bool block = false);
    virtual void saveAnnotations(const std::string& id, const std::string& filename);
    virtual void loadAnnotations(const std::string& filename,
                                 std::vector<std::string>* paths,
                                 std::vector<int32_t>* annos);
    virtual void saveClassifier(const std::string& id, const std::string& filename);
    virtual void loadClassifier(const std::string& id, const std::string& filename);

  protected:
    virtual std::shared_ptr<QueryIfo> getQueryIfo_(const std::string& id);

    virtual void train_(const std::string& id);
    virtual void rank_(const std::string& id);

    virtual void addTrsFromFile_(const std::string& id, const std::vector<std::string>& paths);

    std::map<std::string, std::shared_ptr<QueryIfo> > queries_;

    cv::Mat dset_feats_;
    std::vector<std::string> dset_paths_;
    std::string dset_base_path_;
    std::map<std::string, size_t> dset_paths_index_;

    cv::Mat neg_feats_;
    std::vector<std::string> neg_paths_;
    std::string neg_base_path_;
    std::string image_cache_path_;

    featpipe::CaffeEncoder encoder_;
    std::shared_ptr<StatusNotifier> notifier_;

  private:
    cv::Mat
    computeFeat_(const std::string& imfile);

    void
    process(const std::string& id, const std::string& imfile);
  };

}

#endif
