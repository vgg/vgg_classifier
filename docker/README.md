vgg_classifier service (docker)
===============================

Author:

 + Ernesto Coto, University of Oxford – <ecoto@robots.ox.ac.uk>

#### *About these instructions*

The instructions below are meant to be executed on a computer with the Linux OS. However, similar instructions can be used for macOS or MS Windows (with or without "sudo", changing the path-separators and using another folder different to $HOME).

#### *Downloading*

The docker version of the service can be found in the VGG docker repository at Docker Hub. It can be downloaded via the terminal by entering:

	docker pull oxvgg/vgg_classifier

After the download you should have a docker image called `oxvgg/vgg_classifier` on your computer.

#### *Sample data*

Sample data can be found at http://www.robots.ox.ac.uk/~vgg/software/vic/downloads/data/vic_data_COCO_v2.tar (or http://www.robots.ox.ac.uk/~vgg/software/vic/downloads/data/vic_data_COCO_v2.zip).

The `vic_data_COCO_v2` file contains a small subset of COCO2014 (100 images), along with an example metadata file and sample negative images. It also contains pre-processed data files for the category search backend, computed out of the sample COCO2014 images. **Please visit http://cocodataset.org/#termsofuse for the terms of use of the COCO dataset**.

For the rest of the instructions, it is assumed that you have decompressed `vic_data_COCO_v2` in your $HOME directory. Therefore, you should have a folder called `vgg` in your $HOME (i.e. $HOME/vgg should exist).

#### *Starting/Stopping the service*

The service will run by default at port 35215.

The service needs access to: 1) the preprocessed data within the `backend_data` folder, and 2) sample training images within the `frontend_data` folder

NOTE: The training images can be in any folder, as long as the container has access to it. We are using the ones in our `frontend_data` sample data just for convenience.

Start the service and let it run in the background via the terminal by entering:

```
sudo docker run --rm -v $HOME/vgg/vic/backend_data:/webapps/visorgen/backend_data:Z -v $HOME/vgg/vic/frontend_data:$HOME/vgg/vic/frontend_data:Z -p 127.0.0.1:35215:35215 -d oxvgg/vgg_classifier /webapps/visorgen/vgg_classifier/start_service.sh
```

This instruction will start a new oxvgg/vgg_classifier container.

The container can be stopped/killed using the docker service.

#### *Testing the services*

You can use the scripts in the `test` folder of this repository for testing the services. Just take into account that the visualization options of the script will not work as no GUI can be displayed inside the docker image. See the README file in the `test` folder for instructions on how to use the test scripts.

#### *Performing a query*

Use the `test_legacy_serve.py` script (found in the `test` folder of this repository) to run a query. Just enter in the terminal window:

	python test_legacy_serve.py <query_string> -p <training-images-folder> -o <output-text-file>

where `<training-images-folder>` is the full path to a folder containing the training images to be used for the query `<query_string>`, and `<output-text-file>` is the full path to a text file where the results of the query will be stored.

For instance, given that you have downloaded the sample data, you should be able to run

	python test_legacy_serve.py car -p $HOME/vgg/vic/frontend_data/curatedtrainimgs/cpuvisor-srv/curated__{#car}/positive -o results.txt

The results of the query will be stored in JSON format in `results.txt` in the same directory where the script was executed from.

Note that given the default configuration at https://gitlab.com/vgg/vgg_classifier/blob/master/config.prototxt, a maximum of 10000 results will be returned (as per the `page_size` setting).

**NOTE**: For MS Windows users, the path with which `test_legacy_serve.py` is executed will be incompatible with the Linux OS inside the docker container. Therefore, we suggest you modify `test_legacy_serve.py` slightly (step 3) to convert the paths to Linux-style and redirect them to the folder where the images will be found INSIDE the docker container.

#### *BATCH Data ingestion*

A complete new `BATCH` of new images can be ingested by the service. Please follow the instructions below:

1. Remove the previous files at $HOME/vgg/mydata/images/mydataset/ (but not the folder itself) and place the new files in that same folder.
2. Remove the preprocessed data files from the sample data, i.e, all files starting with `dsetfeats` at $HOME/vgg/vic/backend_data/cpuvisor-srv
3. Create a new file called `dsetpaths.txt` in $HOME/vgg/vic/backend_data/cpuvisor-srv containing the paths to the files to be ingested. The paths should be relative to $HOME/vgg/mydata/images/mydataset/. You will find an example in $HOME/vgg/mydata/dsetpaths.sample.txt
4. Run the following command in the terminal window:

```
sudo docker run --rm -v $HOME/vgg/vic/backend_data:/webapps/visorgen/backend_data:Z -v $HOME/vgg/mydata/images/mydataset:/webapps/visorgen/datasets/images/mydataset:Z -it oxvgg/vgg_classifier /webapps/visorgen/vgg_classifier/bin/cpuvisor_preproc -config_path /webapps/visorgen/vgg_classifier/config.prototxt -dsetfeats
```

The data ingestion process should start and you should be able to follow it with the printouts on the screen.

You can also leave the process running in the background by replacing the `-it` option in the command above by `-d`. The container should be removed automatically once the process is finished.

After the data ingestion is finished, you will notice that a file called `dsetfeats.binaryproto` has been created in $HOME/vgg/vic/backend_data/cpuvisor-srv.

Please note that every time you perform the data ingestion, you will need to restart the `vgg_classifier` service for the changes to take effect.

#### *Negative images*

New NEGATIVE images can be ingested by the service as well. Please follow the instructions below:

1. Remove the previous files at $HOME/vgg/mydata/negatives/mydataset/ (but not the folder itself) and place the new files in that same folder.
2. Remove the preprocessed data files from the sample data, i.e, all files starting with `negfeats` at $HOME/vgg/vic/backend_data/cpuvisor-srv
3. Create a new file called `negpaths.txt` in $HOME/vgg/vic/backend_data/cpuvisor-srv containing the paths to the files to be ingested. The paths should be relative to $HOME/vgg/mydata/negatives/mydataset/. You will find an example in $HOME/vgg/mydata/negpaths.sample.txt
4. Run the following command in the terminal window:

```
sudo docker run --rm -v $HOME/vgg/vic/backend_data:/webapps/visorgen/backend_data:Z -v $HOME/vgg/mydata/negatives/mydataset:/webapps/visorgen/datasets/negatives/mydataset:Z -it oxvgg/vgg_classifier /webapps/visorgen/vgg_classifier/bin/cpuvisor_preproc -config_path /webapps/visorgen/vgg_classifier/config.prototxt -negfeats
```

The ingestion of negative images should start and you should be able to follow it with the printouts on the screen.

You can also leave the process running in the background by replacing the `-it` option in the command above by `-d`. The container should be removed automatically once the process is finished.

After the ingestion of negative images is finished, you will notice that a file called `negfeats.binaryproto` has been created in $HOME/vgg/vic/backend_data/cpuvisor-srv.

Please note that every time you perform the ingestion of negative images, you will need to restart the `vgg_classifier` service for the changes to take effect.

#### *INCREMENTAL Data ingestion*

If you are planning to ingest different chunks of your dataset at different dates or times you can perform the ingestion `incrementally`. This is useful when, for instance:

 + The data ingestion is performed in parallel threads, with each thread ingesting a different part of the file list in the `dsetpaths.txt` file.
 + A batch of files is ingested, and then you want to add more files at a later date.

If this is your case you need to perform the data ingestion `incrementally` rather than in a complete `batch`. This is done by creating separate `batch` files for each chunk of data that you want to process, instead of having just one `batch` file for the complete dataset. The incremental approach uses a global `index` containing the list of `batch` files you have processed so far, allowing you to add more `batch` files in the future by just updating the `index` file.

As an example, let's perform the same BATCH data ingestion we did before (100 images) in two chunks of 50 images. Please follow the next steps:

1. First, if you have performed the steps in the *__BATCH Data Ingestion__* section explained before, please delete $HOME/vgg/vic/backend_data/cpuvisor-srv/dsetfeats.binaryproto. This has to be done because the incremental approach will use `dsetfeats.binaryproto` as the `index`. You only need to do this if you have followed the instructions in the section called *__BATCH Data Ingestion__*. **If you are adding more chunks, DO NOT DELETE THE FILE!**.
2. If you have followed the steps in the *__BATCH Data Ingestion__* section explained before, you should have a file called `dsetpaths.txt` in $HOME/vgg/vic/backend_data/cpuvisor-srv containing the paths to 100 files to be ingested. If you do not have it, please copy it from $HOME/vgg/mydata/dsetpaths.sample.txt and change the file name to `dsetpaths.txt`.
3. Let's create the first `batch` file for the first chunk of images. We are going to run the same command we used in the *__BATCH Data Ingestion__* section, but this time specifying that we just want to process one part of the list of files. We can do that by specifying indexes in the list of files. The `startidx` is inclusive and starts from 0. The `endidx` is exclusive, so you should always add 1 to that index so that the last file in the chunk is processed. So, since we want to process the first 50 images we need to specify the range [0,49], 0 for the `startidx` and 50 for the `endidx` (remember this last one is `exclusive`). Run the following command to perform this step:

```
sudo docker run --rm -v $HOME/vgg/vic/backend_data:/webapps/visorgen/backend_data:Z -v $HOME/vgg/mydata/images/mydataset:/webapps/visorgen/datasets/images/mydataset:Z -it oxvgg/vgg_classifier /webapps/visorgen/vgg_classifier/bin/cpuvisor_preproc -config_path /webapps/visorgen/vgg_classifier/config.prototxt -dsetfeats -startidx 0 -endidx 50
```

After the process is finished you will notice that a file called `dsetfeats_1-50.binaryproto` has been created in $HOME/vgg/vic/backend_data/cpuvisor-srv.

4. Now we need to add this `batch` file to the `index` file. We can do that by running the following command:

```
sudo docker run --rm -v $HOME/vgg/vic/backend_data:/webapps/visorgen/backend_data:Z -v $HOME/vgg/mydata/images/mydataset:/webapps/visorgen/datasets/images/mydataset:Z -it oxvgg/vgg_classifier /webapps/visorgen/vgg_classifier/bin/cpuvisor_append_chunk -config_path /webapps/visorgen/vgg_classifier/config.prototxt -chunk_file /webapps/visorgen/backend_data/cpuvisor-srv/dsetfeats_1-50.binaryproto
```

After the process is finished you will notice that a file called `dsetfeat.binaryproto` has been created in $HOME/vgg/vic/backend_data/cpuvisor-srv. This is the `index` file and after this step it only contains file `dsetfeats_1-50.binaryproto` in the index.

5. Now, let's create the `batch` file for the second chunk of data. This time we specify indexes for the range [50,99], i.e., 50 for the `startidx` and 100 for the `endidx`.

```
sudo docker run --rm -v $HOME/vgg/vic/backend_data:/webapps/visorgen/backend_data:Z -v $HOME/vgg/mydata/images/mydataset:/webapps/visorgen/datasets/images/mydataset:Z -it oxvgg/vgg_classifier /webapps/visorgen/vgg_classifier/bin/cpuvisor_preproc -config_path /webapps/visorgen/vgg_classifier/config.prototxt -dsetfeats -startidx 50 -endidx 100
```

After the process is finished you will notice that a file called `dsetfeats_51-100.binaryproto` has been created in $HOME/vgg/vic/backend_data/cpuvisor-srv.

6. Now let's add the new `batch` file to the `index` file, with the command:

```
sudo docker run --rm -v $HOME/vgg/vic/backend_data:/webapps/visorgen/backend_data:Z -v $HOME/vgg/mydata/images/mydataset:/webapps/visorgen/datasets/images/mydataset:Z -it oxvgg/vgg_classifier /webapps/visorgen/vgg_classifier/bin/cpuvisor_append_chunk -config_path /webapps/visorgen/vgg_classifier/config.prototxt -chunk_file /webapps/visorgen/backend_data/cpuvisor-srv/dsetfeats_51-100.binaryproto
```

After the process is finished the `index` file will contain references to two files: `dsetfeats_1-50.binaryproto` and `dsetfeats_51-100.binaryproto`.

When the `vgg_classifier` service is started, it will detect that $HOME/vgg/vic/backend_data/cpuvisor-srv/dsetfeats.binaryproto is an `index` file and it will load all `batch` files referenced on it.

Please note that every time you perform the data ingestion of a chunk, you will need to restart the `vgg_classifier` service for the changes to take effect.
