import os
import sys
import argparse
import json
from time import sleep

# Add path to pyclient so that it can be imported
file_dir = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(file_dir, '..'))
from pyclient import visor_client, visor_notifier

# Useful default values for parameters
DEFAULT_PRINT_NOTIFICATIONS_FLAG = False
DEFAULT_VISUALISE_RESULTS_FLAG = False
DEFAULT_RESULTS_TIMEOUT = 20
DEFAULT_MAX_NUM_RESULTS = 36
DEFAULT_TXT_OUTPUT_FILE = None


def recv_notification(notification):
    """
        Simple notification stdout printer
        Parameters:
            notification: notification text string
    """
    print ('\n++++++++++++++++++')
    print (notification)
    print ('++++++++++++++++++')


if __name__ == "__main__":
    """ Main method """

    # Parse arguments
    parser = argparse.ArgumentParser(description='cpuvisor_service test script')
    parser.add_argument('query_string', metavar='query_string', type=str,
        help='Query string. Make sure it does not contain special characters and enclose queries of multiple words with quotes')
    parser.add_argument('-t', dest='results_timeout',
        default=DEFAULT_RESULTS_TIMEOUT, type=int,
        help='Number of seconds to wait for the query results to be produced. Default: %i' % DEFAULT_RESULTS_TIMEOUT)
    parser.add_argument('-m', dest='max_num_results',
        default=DEFAULT_MAX_NUM_RESULTS, type=int,
        help='Maximum number of results to be printed (or visualized) to stdout. Default: %i' % DEFAULT_MAX_NUM_RESULTS)
    parser.add_argument('-o', dest='output_file',
        type=str, default=DEFAULT_TXT_OUTPUT_FILE,
        help='Full path to text output file. It ignores the -m parameter and saves all results in the first results page. Default: %s' % str(DEFAULT_TXT_OUTPUT_FILE))
    parser.add_argument('-n', dest='print_notifications',
        default=DEFAULT_PRINT_NOTIFICATIONS_FLAG, action= 'store_true',
        help='If used, notifications on different stages of the test process will be printed to stdout. Default: Disable')
    parser.add_argument('-v', dest='visualize_results',
        default=DEFAULT_VISUALISE_RESULTS_FLAG, action= 'store_true',
        help='If used, the final results will be displayed in a GUI using matplotlib. Default: Disable')
    parser.add_argument('config_file', type=str,
        help='Full path to Google Protobuf text file, containing the configuration for the cpuvisor_service.')
    args = parser.parse_args()

    # 1) Connect to cpuvisor_service

    print ('** Connecting to server for query:', args.query_string)
    print ('** Using the configuration file at:', args.config_file)
    client = visor_client.VisorClient(args.config_file)

    # 2) (Optional) create a notifier class to subscribe to notifications.
    #    These are sent when:
    #       - a query changes state
    #       - an image finishes downloading/reading + processing
    #       - all images finish downloading/reading + processing

    if args.print_notifications:
        notifier = visor_notifier.VisorNotifier(args.config_file, recv_notification)

    # 3) Start a query by getting a query ID

    print ('** Getting Query ID...')
    query_id = client.start_query()
    print ('** Query ID is: %s'  % query_id)

    # 4) Add training samples from a local directory.  The script will
    #      attempt to load the images from a folder with the same name
    #      as the query_string. The folder must be located inside the
    #      'image_cache_path' specified in the configuration file.

    pos_trs_dir = os.path.join(client.config.server_config.image_cache_path,
                               args.query_string)
    print ('** Adding Training Samples from:', pos_trs_dir)
    pos_trs_paths = [os.path.join(pos_trs_dir, pos_trs_fname) for
                     pos_trs_fname in os.listdir(pos_trs_dir)]
    for pos_trs_path in pos_trs_paths:
        print ('Adding', pos_trs_path)
        client.add_trs_from_file(query_id, pos_trs_path)

    # 5) Wait a bit for for training samples to be acquired and their features computed

    print ('** Starting training sample addition process...')
    sleep(args.results_timeout)

    # 6) Train the classifier and get ranked results

    print ('** Sending train + rank commands...')

    # Training and ranking function operates in blocking manner,
    # stopping processing of all outstanding training images
    ranking = client.train_rank_get_ranking(query_id)

    print ('** Results page size from configuration file is:', client.config.server_config.page_size)
    print ('** Retrieved %d results from page %d' % (len(ranking.rlist), ranking.page))
    print ('** Total number of pages:', ranking.page_count)

    # One can also get subsequent ranking pages. The example below
    # retrieves the results from the second page
    #ranking = client.get_ranking(query_id, 2)

    # 7) Free the query in the cpuvisor_service to save memory
    client.free_query(query_id)

    # 8) (Optional) Save results to text file

    if args.output_file:
        print ('** Saving %d results to %s' %(len(ranking.rlist), args.output_file))
        with open(args.output_file, 'w+') as outfile:
            ctr = 1
            for ritem in ranking.rlist:
                outfile.write('%d: %s (%f)\n' % (ctr, ritem.path, ritem.score))
                ctr = ctr + 1

    # 9) Print the top-ranking results, up to a limit

    # clamp maximum number of results by the length of the results page
    max_display_results = min(args.max_num_results, len(ranking.rlist))
    print ('** Only showing the first %d results:' % max_display_results)
    ctr = 1
    for ritem in ranking.rlist:
        print ('%d: %s (%f)' % (ctr, ritem.path, ritem.score))
        ctr = ctr + 1
        if ctr > max_display_results:
            break

    # 10) (Optional) Visualise ranked results in a GUI. 18 images per window.

    if args.visualize_results:
        import matplotlib.pyplot as plt
        import matplotlib.image as mpimg

        num_pages_for_visualization = int(max_display_results/(3*6))
        for i in range(num_pages_for_visualization):

            rlist_plt = ranking.rlist[(i*3*6):(i*3*6)+(3*6)]

            fig, axes = plt.subplots(3, 6, figsize=(12, 6),
                                     subplot_kw={'xticks': [], 'yticks': []})
            fig.subplots_adjust(hspace=0.3, wspace=0.05)

            for ax, ritem in zip(axes.flat, rlist_plt):
                im = mpimg.imread(os.path.join(client.config.preproc_config.dataset_im_base_path, ritem.path))
                ax.imshow(im)
                ax.set_title(ritem.score)

            plt.show()
            # plt.show(block=(i>=num_pages_for_visualization-1)) # this will only block the last page

        if max_display_results % (3*6) > 0 :

            max_display_results = max_display_results % (3*6)
            i = max(0, num_pages_for_visualization-1)
            rlist_plt = ranking.rlist[(i*3*6):(i*3*6)+max_display_results]

            fig, axes = plt.subplots(3, 6, figsize=(12, 6),
                                     subplot_kw={'xticks': [], 'yticks': []})
            fig.subplots_adjust(hspace=0.3, wspace=0.05)

            for ax, ritem in zip(axes.flat, rlist_plt):
                im = mpimg.imread(os.path.join(client.config.preproc_config.dataset_im_base_path, ritem.path))
                ax.imshow(im)
                ax.set_title(ritem.score)

            plt.show()

