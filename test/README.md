vgg_classifier test scripts
===========================

Author:

 + Ernesto Coto, University of Oxford – <ecoto@robots.ox.ac.uk>

#### *About the test scripts*

As mentioned in the [Usage](https://gitlab.com/vgg/vgg_classifier#usage) section, you need to run the `legacy_serve.py` script as well as the `cpuvisor_service` executable to start the backend service that interfaces with the [vgg_frontend](https://gitlab.com/vgg/vgg_frontend) web application. The two python scripts in this folder will test both components.

However, **the scripts can also be seen as examples of usage of the vgg_classifier service**. You could use both components and develop your own web frontend, or you can also skip the `legacy_serve.py` script and interface directly with the `cpuvisor_service`. See more details below.

#### *Testing cpuvisor_service*

By using the `test_cpuvisor_service.py` you can run a simple query to the `cpuvisor_service` executable. The test skips the API used by the [vgg_frontend](https://gitlab.com/vgg/vgg_frontend) web application, so executing the `legacy_serve.py` script is not necessary.

In order to use it, please start the `cpuvisor_service` executable, which will start a service waiting to be contacted for a query. Then, in a separate window, execute:

```
python test_cpuvisor_service.py <query_string>
```

where `<query_string>` corresponds to the category you want to build the classifier for. Make sure `<query_string>` does not contain special characters and enclose queries of multiple words with quotes.

Now, for building the classifier, the `cpuvisor_service` needs sample (training) images of the category. When running the aforementioned command, the script will try to read the Google Protobuf text configuration file of `cpuvisor_service` (see step 1 in [Usage](https://gitlab.com/vgg/vgg_classifier#usage); by default the script will try to load a file called `config.prototxt`), to get the value of the `image_cache_path` variable. The script will try to load the images from a subfolder called `<query_string>` inside the path specified by `image_cache_path`. If the images are not found, the script will fail to build the classifier.

Display the full help of `test_cpuvisor_service.py` with:

```
python test_cpuvisor_service.py -h
```

You will see additional arguments that can be useful, such as displaying the results in a GUI, saving the results to a text file, etc.


#### *Testing legacy_serve*

The `legacy_serve.py` script starts a server that interfaces with the `cpuvisor_service` and with the [vgg_frontend](https://gitlab.com/vgg/vgg_frontend) web application. So, `legacy_serve.py` serves as a middleware. You can develop your own web frontend and communicate with `legacy_serve.py`, which in turn will communicate with `cpuvisor_service`.

By using the `test_legacy_serve.py` you can run a simple query. In order to use it, please start `cpuvisor_service` and `legacy_serve.py` as described in the [Usage](https://gitlab.com/vgg/vgg_classifier#usage). Then, in a separate window, execute:

```
python test_legacy_serve.py <query_string>
```

where `<query_string>` corresponds to the category you want to build the classifier for. Make sure `<query_string>` does not contain special characters and enclose queries of multiple words with quotes.

Now, for building the classifier, the `cpuvisor_service` needs sample (training) images of the category. When running the aforementioned command, the script will try to read the Google Protobuf text configuration file of `cpuvisor_service` (see step 1 in [Usage](https://gitlab.com/vgg/vgg_classifier#usage); by default the script will try to load a file called `config.prototxt`), to get the value of the `image_cache_path` variable. The script will try to load the images from a subfolder called `<query_string>` inside the path specified by `image_cache_path`. If the images are not found, the script will fail to build the classifier.

If you do not want to depend on the configuration file of `cpuvisor_service`, you can also just supply your own path for the sample images, using the `-p` argument. For instance:

```
python test_legacy_serve.py <query_string> -p /tmp/training_images/
```

The `-p` argument will override the loading of the `cpuvisor_service` configuration file, and the script will attempt to load the images from the specified folder. If the images are not found, the script will fail to build the classifier.

Display the full help of `test_legacy_serve.py` with:

```
python test_legacy_serve.py -h
```

You will also see other additional arguments that can be useful, such as displaying the results in a GUI, saving the results to a text file, etc.
