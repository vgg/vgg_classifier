import os
import sys
import argparse
import socket
import json

# Add path to pyclient so that it can be imported
file_dir = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(file_dir, '..'))
import pyclient

# Useful default values for parameters
DEFAULT_VISUALISE_RESULTS_FLAG = False
DEFAULT_MAX_NUM_RESULTS = 36
DEFAULT_PATH_TO_SAMPLES = None
DEFAULT_TXT_OUTPUT_FILE = None

# Network communication constants
SERVE_IP = '127.0.0.1'
SERVE_PORT = 35215
BUFFER_SIZE = 1024
TCP_TERMINATOR = '$$$'


def send_req_obj(socket, req_obj):
    """
        Sends a request to the server
        Parameters:
            socket: Socket used to connect to the server. The socket
                    should be connected to SERVE_PORT at SERVE_IP
            req_obj: Dictionary containing the parameters of the request.
                    - 'func' specifies the function to be executed
                    - The rest of the parameters will correspond to the
                      parameters of 'func', in order
    """
    message = json.dumps(req_obj) + TCP_TERMINATOR
    socket.send(message.encode())


def recv_rep_obj(socket):
    """
        Receives a request response from the server.
        It waits until a response is received. It receives up to
        BUFFER_SIZE bytes. Any response should be finished with
        TCP_TERMINATOR to recognize the end of it.
        Parameters:
            socket: Socket used to connect to the server. The socket
                    should be connected to SERVE_PORT at SERVE_IP
        Returns:
            a JSON object with the response

    """
    rep_data = socket.recv(BUFFER_SIZE)
    rep_data = rep_data.decode()
    term_idx = rep_data.find(TCP_TERMINATOR)
    while term_idx < 0:
        append_data = socket.recv(BUFFER_SIZE)
        append_data = append_data.decode()
        if not append_data:
            raise RuntimeError("No data received!")
        else:
            rep_data = rep_data + append_data
            term_idx = rep_data.find(TCP_TERMINATOR)

    rep_data = rep_data[0:term_idx]

    return json.loads(rep_data)


if __name__ == "__main__":
    """ Main method """

    # Parse arguments
    parser = argparse.ArgumentParser(description='legacy wrapper test script')
    parser.add_argument('query_string', metavar='query_string', type=str,
        help='Query string. Make sure it does not contain special characters and enclose queries of multiple words with quotes')
    parser.add_argument('-p', dest='path_to_sample_images',
        type=str, default=DEFAULT_PATH_TO_SAMPLES,
        help='Full path to input training sample images. This parameter OVERRIDES the -c parameter becuase it avoids the need of a config_file. Default: %s' % str(DEFAULT_PATH_TO_SAMPLES))
    parser.add_argument('-o', dest='output_file',
        type=str, default=DEFAULT_TXT_OUTPUT_FILE,
        help='Full path to text output file. It ignores the -m parameter and saves all results in the first results page. Default: %s' % str(DEFAULT_TXT_OUTPUT_FILE))
    parser.add_argument('-m', dest='max_num_results',
        default=DEFAULT_MAX_NUM_RESULTS, type=int,
        help='Maximum number of results to be printed (or visualized) to stdout. Default: %i' % DEFAULT_MAX_NUM_RESULTS)
    parser.add_argument('-v', dest='visualize_results',
        default=DEFAULT_VISUALISE_RESULTS_FLAG, action= 'store_true',
        help='If used, the final results will be displayed in a GUI using matplotlib. Default: Disable')
    parser.add_argument('config_file', type=str,
        help='Full path to Google Protobuf text file, containing the configuration for the cpuvisor_service.')
    args = parser.parse_args()

    # 1) Connect to legacy wrapper and test connection

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((SERVE_IP, SERVE_PORT))

    print ('** Sending selfTest')
    req_obj = {'func': 'selfTest'}
    send_req_obj(s, req_obj)
    print ('Received response:')
    rep_obj = recv_rep_obj(s)
    print (rep_obj)

    # 2) Start a query by getting a query ID

    print ('** Sending getQueryId')
    req_obj = {'func': 'getQueryId'}
    send_req_obj(s, req_obj)
    print ('Received response:')
    rep_obj = recv_rep_obj(s)
    print (rep_obj)
    query_id = rep_obj['query_id']

    # 3) Add training samples from a local directory
    #    - If the -p parameter was used, the script will attempt to load
    #      the images from the specified folder.
    #    - Otherwise, the script will attempt to load the images from a
    #      folder with the same name as the query_string. The folder must
    #      be located inside the 'image_cache_path' specified in the
    #      configuration file.

    if args.path_to_sample_images:
        pos_trs_dir = args.path_to_sample_images
    else:
        import google.protobuf.text_format
        from proto import cpuvisor_config_pb2 as protoconfig
        # read in configuration
        config = protoconfig.Config()
        with open(args.config_file, 'r') as f:
            google.protobuf.text_format.Merge(f.read(), config)
        pos_trs_dir = os.path.join(config.server_config.image_cache_path,
                      args.query_string)

    pos_trs_paths = [os.path.join(pos_trs_dir, pos_trs_fname) for
                        pos_trs_fname in os.listdir(pos_trs_dir)]

    for pos_trs_path in pos_trs_paths:
        print ('** Sending addPosTrs ', pos_trs_path)
        req_obj = {'func': 'addPosTrs',
                   'query_id': query_id,
                   'impath': pos_trs_path}
        send_req_obj(s, req_obj)
        print ('Received response:')
        rep_obj = recv_rep_obj(s)
        print (rep_obj)
        if not rep_obj['success']: raise RuntimeError(rep_obj['err_msg'])

    # 4) Train the classifier

    print ('** Sending train')
    req_obj = {'func': 'train',
               'query_id': query_id}
    send_req_obj(s, req_obj)
    print ('Received response:')
    rep_obj = recv_rep_obj(s)
    print (rep_obj)
    if not rep_obj['success']: raise RuntimeError(rep_obj['err_msg'])

    # 5) Rank results

    print ('** Sending rank')
    req_obj = {'func': 'rank',
               'query_id': query_id}
    send_req_obj(s, req_obj)
    print ('Received response:')
    rep_obj = recv_rep_obj(s)
    print (rep_obj)
    if not rep_obj['success']: raise RuntimeError(rep_obj['err_msg'])

    # 6) Get ranked results

    print ('** Sending getRanking')
    req_obj = {'func': 'getRanking',
               'query_id': query_id}
    send_req_obj(s, req_obj)
    print ('Received response:')
    rep_obj = recv_rep_obj(s)
    #print (rep_obj) # avoid printing a very long output
    if not rep_obj['success']: raise RuntimeError(rep_obj['err_msg'])
    rank_result = rep_obj
    print ('Retrieved %d results' % rank_result['total_len'])

    # 7) Free the query in the cpuvisor_service to save memory

    print ('** Sending releaseQueryId')
    req_obj = {'func': 'releaseQueryId',
               'query_id': query_id}
    send_req_obj(s, req_obj)
    print ('Received response:')
    rep_obj = recv_rep_obj(s)
    print (rep_obj)
    if not rep_obj['success']: raise RuntimeError(rep_obj['err_msg'])

    # 8) Disconnect

    print ('** Disconnecting')
    s.close()

    # 9) (Optional) Save results to text file

    if args.output_file:
        print ('** Saving %d results to %s' %(rank_result['total_len'], args.output_file))
        with open(args.output_file, 'w+') as outfile:
            json.dump(rank_result['ranklist'], outfile, indent=2)

    # 10) Print the top-ranking results, up to a limit

    # clamp maximum number of results by the length of the results page
    max_display_results = min(args.max_num_results, len(rank_result['ranklist']))
    print ('** Only showing the first %d results:' % max_display_results)
    ctr = 1;
    for ritem in rank_result['ranklist']:
        print ('%d: %s (%f)' % (ctr, ritem['image'], ritem['score']))
        ctr = ctr + 1
        if ctr > max_display_results:
            break

    # 11) (Optional) Visualise ranked results in a GUI. 18 images per window.

    if args.visualize_results:
        import matplotlib.pyplot as plt
        import matplotlib.image as mpimg

        num_pages_for_visualization = int(max_display_results/(3*6))
        for i in range(num_pages_for_visualization):

            rlist_plt = rank_result['ranklist'][(i*3*6):(i*3*6)+(3*6)]

            fig, axes = plt.subplots(3, 6, figsize=(12, 6),
                                     subplot_kw={'xticks': [], 'yticks': []})
            fig.subplots_adjust(hspace=0.3, wspace=0.05)

            for ax, ritem in zip(axes.flat, rlist_plt):
                im = mpimg.imread(ritem['image'])
                ax.imshow(im)
                ax.set_title(ritem['score'])

            plt.show()
            # plt.show(block=(i>=num_pages_for_visualization-1)) # this will only block the last page

        if max_display_results % (3*6) > 0 :

            max_display_results = max_display_results % (3*6)
            i = max(0, num_pages_for_visualization-1)
            rlist_plt = rank_result['ranklist'][(i*3*6):(i*3*6)+max_display_results]

            fig, axes = plt.subplots(3, 6, figsize=(12, 6),
                                     subplot_kw={'xticks': [], 'yticks': []})
            fig.subplots_adjust(hspace=0.3, wspace=0.05)

            for ax, ritem in zip(axes.flat, rlist_plt):
                im = mpimg.imread(ritem['image'])
                ax.imshow(im)
                ax.set_title(ritem['score'])

            plt.show()
