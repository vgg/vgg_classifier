#!/bin/bash

# - This script has been tested with macOS Sierra v10.13.3
# - It assumes Homebrew is available in the system (https://brew.sh/).
# - Make sure you have administrator user privileges

#install basic stuff
brew update
brew install wget
brew install cmake
#brew install python #if not already installed (up to python 2.7.13 is supported)

# install pip, which requires sudo access
#wget https://bootstrap.pypa.io/get-pip.py -P /tmp
#sudo python /tmp/get-pip.py

#caffe dependencies
brew install -vd snappy leveldb gflags glog szip lmdb
brew install -vd hdf5 opencv@3
brew install -vd protobuf@3.11
brew install -vd boost@1.57 # the cpp-netlib version used below won't compile with newest boost
brew install -vd openblas
brew install -vd zeromq

#link some stuff
brew link --force boost@1.57
brew link --force opencv@3
brew link --force protobuf@3.11

# download gitlab repo
cd $HOME
wget https://gitlab.com/vgg/vgg_classifier/-/archive/master/vgg_classifier-master.zip -O /tmp/vgg_classifier.zip
unzip /tmp/vgg_classifier.zip
mv vgg_classifier* vgg_classifier
rm -rf /tmp/vgg_classifier.zip

# setup folders
mkdir $HOME/vgg_classifier/dependencies
mkdir $HOME/vgg_classifier/downloaded

# download caffe
wget https://github.com/BVLC/caffe/archive/1.0.zip
unzip 1.0.zip -d $HOME/vgg_classifier/dependencies/

# download cpp-netlib
wget https://github.com/kencoken/cpp-netlib/archive/0.11-devel.zip
unzip 0.11-devel.zip -d $HOME/vgg_classifier/dependencies

# download liblinear
wget https://github.com/cjlin1/liblinear/archive/v210.zip
unzip v210.zip -d $HOME/vgg_classifier/dependencies

# remove zips
rm 0.11-devel.zip 1.0.zip v210.zip

# compile caffe
mv $HOME/vgg_classifier/dependencies/caffe* $HOME/vgg_classifier/dependencies/caffe
cd $HOME/vgg_classifier/dependencies/caffe/
cp Makefile.config.example Makefile.config
sed -i '.sed' 's/# CPU_ONLY/CPU_ONLY/g' Makefile.config
sed -i '.sed' 's/# OPENCV_VERSION := 3/OPENCV_VERSION := 3/g' Makefile.config # homebrew will install opencv3
sed -i '.sed' 's/BLAS := atlas/BLAS := open/g' Makefile.config
sed -i '.sed' 's/# BLAS_INCLUDE := $(/BLAS_INCLUDE := $(/g' Makefile.config
sed -i '.sed' 's/# BLAS_LIB := $(/BLAS_LIB := $(/g' Makefile.config
sed -i '.sed' 's/# PYTHON_INCLUDE +=/PYTHON_INCLUDE +=/g' Makefile.config
sed -i '.sed' 's/# Configure build/CXXFLAGS += -std=c++11/g' Makefile
make all

# register the openblas directory for compiling the vgg_classifier
OPENBLAS_DIR=$(brew --prefix openblas)

# compile cpp-netlib
# This compilation currently breaks because of openssl@1.1
cd  $HOME/vgg_classifier/dependencies/cpp-netlib-0.11-devel/
mkdir build
cd build
cmake -DOPENSSL_INCLUDE_DIR=/usr/local/opt/openssl/include -DOPENSSL_SSL_LIBRARY=/usr/local/opt/openssl/lib/libssl.dylib ../
make

# compile liblinear
cd $HOME/vgg_classifier/dependencies/liblinear-210/
make lib
ln -s liblinear.so.3 liblinear.so

#if the C++ ZMQ port is not installed, vgg_classifier won't compile.
#do this to obtain it.
wget https://raw.githubusercontent.com/zeromq/cppzmq/master/zmq.hpp
cp zmq.hpp /usr/local/include/

# vgg_classifier additional dependencies
pip install protobuf==3.1.0
pip install gevent==1.1.0 greenlet==0.4.15 pyzmq==17.1.2
pip install matplotlib==1.5.3

# compile and install vgg_classifier
cd $HOME/vgg_classifier/
mkdir build
cd build
cmake -DCMAKE_CXX_STANDARD=11 \
      -DCaffe_DIR=$HOME/vgg_classifier/dependencies/caffe \
      -DCaffe_INCLUDE_DIR="$HOME/vgg_classifier/dependencies/caffe/include;$HOME/vgg_classifier/dependencies/caffe/build/src;$OPENBLAS_DIR/include" \
      -DLiblinear_DIR=$HOME/vgg_classifier/dependencies/liblinear-210/ \
      -Dcppnetlib_DIR=$HOME/vgg_classifier/dependencies/cpp-netlib-0.11-devel/build/ ../
make
make install

#add dylb paths to vgg_classifier dependencies
echo "export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:$HOME/vgg_classifier/dependencies/liblinear-210:$HOME/vgg_classifier/dependencies/caffe/build/lib" >> $HOME/.profile
